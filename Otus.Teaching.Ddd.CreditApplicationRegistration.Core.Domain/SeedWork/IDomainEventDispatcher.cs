﻿using System.Threading.Tasks;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.SeedWork
{
    public interface IDomainEventDispatcher
    {
        void Dispatch(DomainEvent domainEvent);
        
        Task DispatchAsync(DomainEvent domainEvent);
    }
}