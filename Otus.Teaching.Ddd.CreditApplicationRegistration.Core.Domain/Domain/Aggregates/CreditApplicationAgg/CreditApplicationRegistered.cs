﻿using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Aggregates.CreditApplicationAgg
{
    public class CreditApplicationRegistered
          : DomainEvent
    {
        public Guid CreditApplicationId { get; set; }
    }
}
