﻿using System;
using Otus.Teaching.Ddd.Contract;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.SeedWork;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Aggregates.CreditApplicationAgg
{
    /// <summary>
    /// Кредитная заявка
    /// </summary>
    public class CreditApplication
		: DomainEntity, IAggregateRoot
    {
        private decimal BadRate = 15.5m;
        private decimal GoodRate = 7;

        /// <summary>
        /// Клиент
        /// </summary>
        public Customer Customer { get; private set; }

	    /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedDate { get; private set; }
	
        /// <summary>
        /// Статус
        /// </summary>
        public CreditApplicationStatus Status { get; private set; }

        /// <summary>
        /// Дата обновления статуса
        /// </summary>
        public DateTime StatusDate { get; private set; }

        /// <summary>
        /// Условия кредита
        /// </summary>
        public CreditTerms CreditTerms { get; private set; }

		/// <summary>
		/// Автор заявки
		/// </summary>
		public Employee Author { get; private set; }

        public CreditApplication(Customer customer, CreditTerms terms, Employee author, Guid id)
            : base(id)
		{
            Customer = customer;
            CreditTerms = terms;
            Author = author;
            Status = CreditApplicationStatus.New;
            StatusDate = DateTime.Now;
		}

        public void CalculatePossibleRate(bool customerHasOverduePayment)
        {
            if(customerHasOverduePayment)
            {
                CreditTerms.SetPossibleRate(BadRate);
            }            
            else
            {
                CreditTerms.SetPossibleRate(GoodRate);
            }
        }

        public void Register()
        {
            Status = CreditApplicationStatus.Registered;
            StatusDate = DateTime.Now;

            AddDomainEvent(new CreditApplicationRegistered()
            {
                 CreditApplicationId = this.Id
            });
        }

        protected CreditApplication()
        {

        }
    }
}

