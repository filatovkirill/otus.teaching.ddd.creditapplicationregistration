﻿using System;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.SeedWork;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Aggregates.CreditApplicationAgg
{
    public class Contact
        : IValueObject
    {
        public Email Email { get; private set; }
        
        public Phone Phone { get; private set; }
        
        public Contact(Email email, Phone phone)
        {
            Email = email ?? throw new ArgumentNullException(nameof(email));
            Phone = phone ?? throw new ArgumentNullException(nameof(phone));
        }

        protected Contact()
        {
            
        }
    }
}